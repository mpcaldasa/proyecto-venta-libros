from django.apps import AppConfig


class AuthlibreriaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'AuthLibreria'
