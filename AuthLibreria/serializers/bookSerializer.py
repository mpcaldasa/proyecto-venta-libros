"""from rest_framework import serializers
from AuthLibreria.models.book import Books

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Books
        fields = ['isbn', 'title', 'type', 'author', 'creation_date', 'price']"""