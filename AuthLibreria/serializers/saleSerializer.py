"""from rest_framework import serializers
from AuthLibreria.models.sale import Sale

class SaleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sale
        fields = ['venId', 'cliCorreo', 'venFechaCompra', 'venValorTotal']"""