"""from rest_framework import status, views
from rest_framework.decorators import api_view
from rest_framework.response import Response
from AuthLibreria.serializers.bookSerializer import BookSerializer
from AuthLibreria.models.book import Books
from rest_framework.parsers import JSONParser 
from django.http.response import JsonResponse

class BooksView(views.APIView):

    @api_view(['GET', 'POST'])
    def book_list(request, pk):

        book = Books.objects.get(pk=pk) 

        if request.method == 'GET':
            books = Books.objects.all()
            serializer = BookSerializer(books, many=True)
            return Response(serializer.data)

        elif request.method == 'POST':
            serializer = BookSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        elif request.method == 'PUT': 
            data_book = JSONParser().parse(request) 
            book_serializer = BookSerializer(book, data = data_book) 
            if book_serializer.is_valid(): 
                book_serializer.save() 
                res:any
                return res({'message' : ['Book updated sucessfully', JsonResponse(book_serializer.data)] })
            return JsonResponse(book_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
            
        elif request.method == 'DELETE': 
            book.delete() 
            return Books({'message': 'Book was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)"""
