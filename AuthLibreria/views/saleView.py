"""from rest_framework import status, views
from rest_framework.decorators import api_view
from rest_framework.response import Response
from AuthLibreria.serializers.saleSerializer import SaleSerializer
from AuthLibreria.models.sale import Sale
from rest_framework.parsers import JSONParser 
from django.http.response import JsonResponse

class SaleView(views.APIView):
    @api_view(['GET', 'POST'])
    def sale_list(request, pk):

        sale = Sale.objects.get(pk=pk) 

        if request.method == 'GET':
            sale = Sale.objects.all()
            serializer = SaleSerializer(sale, many=True)
            return Response(serializer.data)

        elif request.method == 'POST':
            serializer = SaleSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)"""