from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from AuthLibreria import views
#from AuthLibreria.views.booksView import BooksView
#from AuthLibreria.views.saleView import SaleView

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    #path('book/', views.BooksView.book_list),
    #path('sale/', views.SaleView.sale_list),
]
