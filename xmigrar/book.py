from django.db import models
import datetime


class Books(models.Model):
    isbn = models.CharField(primary_key=True, default="")
    title = models.CharField('Titulo', max_length=100, blank=False)
    type = models.CharField('Tipo', max_length=40, blank=False)
    author = models.CharField('Autor', max_length=100, blank=False)
    creation_date = models.DateField('FechaIngreso', null=False, default=datetime.datetime.now)
    price = models.DecimalField('Precio', decimal_places=2, max_digits=7, blank=False, default=0)
