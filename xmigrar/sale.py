from django.db import models
from .user import User
import datetime


class Sale(models.Model):
    venId = models.BigAutoField(primary_key=True)
    idClient = models.ForeignKey(User, on_delete=models.CASCADE)
    venFechaCompra = models.DateField('FechaCompra', null=False, default=datetime.datetime.now)
    venValorTotal = models.DateField('ValorTotalVenta', max_length=100, blank=False)
    
